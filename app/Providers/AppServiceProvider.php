<?php

namespace SAPM\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $app = $this->app;

        $app->bind('SAPM\Repositories\ClientRepository', 'SAPM\Repositories\ClientRepositoryEloquent');
        $app->bind('SAPM\Repositories\TaskRepository', 'SAPM\Repositories\TaskRepositoryEloquent');
        $app->bind('SAPM\Repositories\UserRepository', 'SAPM\Repositories\UserRepositoryEloquent');
        $app->bind('SAPM\Repositories\RoleRepository', 'SAPM\Repositories\RoleRepositoryEloquent');
        $app->bind('SAPM\Repositories\CategoryRepository', 'SAPM\Repositories\CategoryRepositoryEloquent');
        $app->bind('SAPM\Repositories\SettingRepository', 'SAPM\Repositories\SettingRepositoryEloquent');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
