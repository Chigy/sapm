<?php

namespace SAPM\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoleRepository
 * @package namespace SAPM\Repositories;
 */
interface RoleRepository extends RepositoryInterface
{
    //
}
