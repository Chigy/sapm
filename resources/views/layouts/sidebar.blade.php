<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/img/default.png") }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{!! auth()->user()->name !!}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> @lang('task.texto.online')</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">@lang('task.text.navigation')</li>
            <li><a href="{!! url('/') !!}"><i class="fa fa-dashboard"></i><span>@lang('task.titulo.administrador')</span></a></li>
            @if( auth()->user()->level() == 10 )
            <li><a href="{!! route('tasks.index') !!}"><i class="fa fa-tasks"></i><span>@lang('task.titulo.tareas')</span></a></li>
            <li><a href="{!! route('clients.index') !!}"><i class="fa fa-user"></i><span>@lang('task.titulo.maestrantes')</span></a></li>
            <li><a href="{!! route('clients.index') !!}"><i class="fa fa-user"></i><span>@lang('task.titulo.diplomados')</span></a></li>
            <li><a href="{!! route('categories.index') !!}"><i class="fa fa-th-list"></i><span>@lang('task.titulo.categorias')</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-users"></i><span>@lang('task.titulo.control_usuarios')</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{!! route('users.index') !!}"><i class="fa fa-circle-o"></i> @lang('task.titulo.usuarios')</a></li>
                    <li><a href="{!! route('roles.index') !!}"><i class="fa fa-circle-o"></i> @lang('task.titulo.roles')</a></li>
                </ul>
            </li>
            {{--@else--}}
            {{--<li><a href="{!! route('tasks') !!}"><i class="fa fa-tasks"></i><span>@lang('task.text.my_tasks')</span></a></li>--}}
            @endif
            @if( auth()->user()->level() == 1)
                <li><a href="{!! route('tasks.index') !!}"><i class="fa fa-tasks"></i><span>@lang('task.titulo.tareas')</span></a></li>
                <li><a href="{!! route('configuration.index') !!}"><i class="fa fa-tasks"></i><span>@lang('task.titulo.configuraciones')</span></a></li>
            @endif
            <li class="header">@lang('task.texto.prioridad')</li>
            @foreach(config('task.priority') as $key => $color)
            <li>
                <a href="javascript:void(0)">
                    <i class="fa fa-circle" style="color: {!! $color !!}"></i> <span>{!! $key !!}</span>
                </a>
            </li>
            @endforeach
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>